# Scaletta W1 D3

Programma

15.00.      ALBERO ROSSO
Presentazione dei ragazzi e di Adriano
Nome / che lavoro vorresti fare nella base che costruiremo su Marte , ci sono tanti lavori ancora da inventare.

Attività 1 Sala Scratch
Come facciamo a comunicare? COMUNICAZIONE /SUONO/ Se ci allontaniamo? (trasmissione- fili. alfabeto Morse)
Scratch le onde/audio

Attività 2 Albero Rosso
Trasmissione ISS (codifica) ADRIANO
link per avvistarla
https://maps.esri.com/rc/sat2/index.html

Attività 3 Zone delle rocce

 Come funziona un antenna? Quali tipi di e forme di antenne
L'orecchio umano ispira le antenne dello spazio
https://www.abc.net.au/science/articles/2009/06/09/2593076.htm
come si trasmette il suono? https://www.nasa.gov/sites/default/files/atoms/files/dsn_poster.pdf

Materiali 
Spaghetti/Scotch/Spago /Metro/ Forbici /Carta e penna /marshmallow o pallina di carta

1. Lasciati ispirare  alle antenne 
della NASA
https://www.jpl.nasa.gov/edu/images/activities/spaghettitower_step1.jpg
https://www.mdscc.nasa.gov/

Towers and antenna system used by the U.S. Navy's radio station NAA in Arlington, Virginia in 1923
https://commons.wikimedia.org/wiki/File:NAA_Arlington,_Virginia_antenna_structure_(1923).gif

ESA 
Cerebros Antenna
https://www.esa.int/ESA_Multimedia/Images/2006/11/Cebreros_Antenna_-_Detail_view_of_the_main_dish_structure
https://www.wired.it/scienza/spazio/2018/06/15/voce-stephen-hawking-spazio/
https://www.esa.int/Enabling_Support/Operations/ESA_Ground_Stations/Cebreros_-_DSA_2


2. Costruisci la struttura in 18 minuti
3. Verifica la struttura appoggiando una pallina di carta
4. Fai uno schizzo su un foglio del tuo progetto e pensa a come potresti migliorarlo
5. Migliora il progetto
6. Misura quanto è alto e confrontarlo con i tuoi compagni
In cosa è differente? Cosa hanno fatto gli altri che tu avresti voluto fare? Cosa non hanno fatto gli altri?

17.45 RIFLESSIONI

Attività 4 Area Giochi
Gioco
Le antenne seguono le missioni
https://spaceplace.nasa.gov/dsn-antennas/en/
https://www.mdscc.nasa.gov/index.php/sched-2/

https://spaceplace.nasa.gov/dsn-game/en/

Risorse
https://www.nasa.gov/directorates/heo/scan/communications/outreach/students/txt_kidszone.html
https://www.jpl.nasa.gov/edu/learn/project/building-with-spaghetti/
https://eyes.nasa.gov/dsn/index.html
https://www.mdscc.nasa.gov/
http://gavrt.lewiscenter.org/
https://osr.org/it/blog/astronomia/comunicazioni-tra-terra-e-spazio-in-una-missione-spaziale/
https://www.telemar.it/it/notizie/come-funziona-internet-nello-spazio

LAser
https://www.ansa.it/canale_scienza_tecnica/notizie/fisica_matematica/2021/05/16/la-nasa-punta-sui-laser-per-comunicazioni-spaziali-piu-veloci-_a4e259f5-ac62-49a7-b8ff-a11d17a113fd.html
https://www.nasa.gov/sites/default/files/atoms/files/lcrd-overviewpresentation.pdf

Raggi X
https://www.focus.it/tecnologia/innovazione/nasa-test-per-la-comunicazione-a-raggi-x-con-la-iss


