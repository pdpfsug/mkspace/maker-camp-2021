# Scaletta W1 D1 Astronave

ricordarsi di cambiare password makercamp2021
12 luglio Day #1 L'Astronave

Presenti
Iscritti
X dovrebbe essere presente al F-Actory

Minorenni
X Samuele Marco Polo  2006
X Federico Ferroni 2009
Enea Cerreto online 2010
x Fabio Pauli Matelica 2009
x Greta Matelica 2011
x RAFFAELE Coppola Marco Polo 2009
?x Marco 2007  mandato SMS (oggi 12 luglio non può venire, da domani sì con PC)
Ettore 2011 online 
Celestino 2008 online 

Maggiorenni
Kalavati 3409848572 (da verificare)
Valeria 3394416953 (docente online)

Facilitatori
Costin
Andrei
Mauro
Beatrice online
Chiara online

Mappa gather.town
Lobby
Albero rosso
Fontana
Area Relax

Stanze
Scratch
Blender
Salottino Musica
Caffè
Studio
Area Panchine
Picnic
Roccette
Sala giochi



15:00 Accoglienza (5')
All'entrata: Andrei
Fai firmare il foglio di presenza
Inviare su gather.town
Online : Beatrice
Fai firmare il foglio di presenza
Inviare su gather.town

Se si tarda inviare a fare un giro su area giochi

15:05 Introduzione (7')
Conduttore: Costin
Tempo: 7 minuti
- Costin Introduzione (che cos' è il makerspace, quali sono i principi, incontreranno altre persone durante questo viaggio, esperti)
Presentazione di chi siamo e cosa facciamo.
Illustrazione del programma e presentazione dei facilitatori.

15:10 Presentazione della missione (3')
Conduttore: Mauro
Tempo: 3 minuti
- Mauro introduce la storia

15:15 Tour piattaforma (15')
Conduttore: Beatrice
Tempo: 15 minuti
Tour per la piattaforma gather town (come muoversi, come interagire, aree)
Ice breaker : Conosciamoci (da scegliere)
Mood barometer: How are you feeling today?
Draw your mood, whiteboard
Qual è il superpotere che vorresti avere se potessi sceglierne uno?
Preparare lavagna con post it:
1. Sei appena atterrato su Marte quale cibo avresti portato e che offriresti ad un alieno?
2. Se ci fosse un impostore sull'astronave come ti comporteresti?
3. Qual è l'ultima cosa che hai imparato?

15.30 Training TIME
50' Scratch
5-7 minuti di Demo con Scratch nella lobby
33' Breakout tables
 Tutorial
 https://scratch.mit.edu/ideas
Tera Movimento WebCAM 
Pico Crea una Storia! 
Pico Altro
10' Show&Tell

16.20   Blender Presentazione e istallazione (10')

16.30 Break (10')

16. 40 WORKING TIME
*** Lobby (l'albero rosso)
Nella lobby presentiamo la sfida  : 
Progettare un'astronave

Dovremo immaginare, progettare e costruire l'astronave che porterà i turisti su Marte.
Ci divideremo in due gruppi di lavoro, prima però proviamo a pensare i requisiti per la missione
- quante persone? Cosa devo portare i turisti con sé?
- in quanto tempo?
- quale propulsione?

Vi condividiamo il seguente sito dove sono mostrate delle astronavi molto veloci.
https://www.focus.it/scienza/spazio/le-10-astronavi-piu-veloci-delluniverso
Osserviamo le astronavi mostrate, troviamo e analizziamo le differenze 

Poi ci dividiamo in due gruppi di lavoro
provare a fare una serie di slide con le immagini delle astronavi, loro devono trovar le informazioni)

*** A gruppi
https://www.focus.it/scienza/spazio/le-10-astronavi-piu-veloci-delluniverso
Far Star" presente nel ciclo delle fondazioni di Asimov.
Harlock Arcadia
Stargate Atlantis e Stargate SG-1 
Franck Herbert nel ciclo di romanzi di "Dune",
https://www.ilpost.it/2015/07/22/astronavi-velocita-classifica/

I partecipanti possono svolgere la sfida su:
Scratch - Scena controllo missione
Blender Costruire un'astronave
KSP (per chi cela)  Progettare/assemblare un'astronave
Disegno  Disegnare un'astronave
Costruzione in cartone - Costruzione astronave

Challenge per Scratch: Animazione in una scena "controllo missione". 
Tavolo Pico  Una scena con degli sprite che interagiscono tra di loro in una stanza di controllo
2. Una scena con degli sprite che interagiscono tra di loro in una stanza di controllo in assenza di gravità
Tavolo Tera Una scena con degli sprite che interagiscono tra di loro in una stanza di controllo con video sensing
Scratch tavolo Tera: 
Video sensing Galleria di esempio
https://scratch.mit.edu/studios/4333395/




17.40 Show & Tell

to do
Definire icebreaker carino (ho chiesto a Angela Sofia)
Definire che cos'è un makerspace , manifesto
Inserire poll per lereflection (ho inserito una board Miro
sistemare i link per la documentazione su gather town
Documentazione  dei progetti e della giornata
https://docs.google.com/presentation/d/1cirPpdUhCIzm6SwS_THHkZTUe0VP0S9BxVquyKObw6o/edit?usp=sharing
mettere una persona doppia all'ingresso
incoraggiare l'uso delle emoticon quando online
definire lobby e nomi delle room
preparare traccia per Mauro
Preparare slides, immagini per Costin


"Idee future"
Facciamo creare una escape room nell'ultimo giorno 
https://www.mysteryescaperoom.com/free-online-escape-room-demo

Poll 
https://www.sli.do/features-live-polling

Attività Costruzione dell'astronave
KSP (Celestino )
Scratch (Costin, Beatrice)
Blender (Andrei, Chiara)
Carta (Beatrice)



- Disegnare

    modulo di comandi (dipende dalla missione)
    equipaggio
    moduli (persone , esperimenti scientifici)
    motori
    carburante  
    Batterie
    Antenne
    Pannelli solari
    RCS- Reaction Wheel per il controllo
    navicella di emergenza
    
Paper   Model
https://science.nasa.gov/astrophysics/universe-spacecraft-paper-models

Attività
https://www.jpl.nasa.gov/edu/teach/tag/search/Rockets
https://www.jpl.nasa.gov/edu/learning-space/

https://www.youtube.com/watch?v=x3EZMMFeCWs&ab_channel=ChannelAwesome

Tema buildings
https://usergeneratededucation.wordpress.com/2015/08/14/team-building-activities-that-support-maker-education-stem-and-steam/
