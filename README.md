# Maker Camp 2021

## Si parla di noi

- [Articolo sul nostro blog](https://pdp.linux.it/appuntamenti-locali/1688/maker-camp-2021-fase-seconda/)
- [Articolo su centro pagina](https://www.centropagina.it/fabriano/fabriano-torna-il-maker-camp-il-laboratorio-dedicato-alle-discipline-steam/?amp=1)
- [Articolo sul sito "con i bambini"](https://percorsiconibambini.it/doors/2021/09/07/torna-il-maker-camp-a-settembre-per-scoprire-coding-design-e-modellazione-3d/)
