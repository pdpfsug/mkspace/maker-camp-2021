## Kerbal Space Program

# Onboarding

1. Installa [KSP](https://www.kerbalspaceprogram.com/)
1. Installa [CKAN](https://forum.kerbalspaceprogram.com/index.php?/topic/197082-ckan-the-comprehensive-kerbal-archive-network-v1304-hubble/)
    - Scarica per windows [ckan.exe](https://github.com/KSP-CKAN/CKAN/releases/download/v1.30.4/ckan.exe) o [ckan.dmg](https://github.com/KSP-CKAN/CKAN/wiki/Installing-CKAN-on-macOS) per macOS.
    - Se l'installazione non funziona installa prima [.NET4.5](https://www.microsoft.com/it-IT/download/confirmation.aspx?id=30653)
    - Maggiori informazioni sul [forum](https://forum.kerbalspaceprogram.com/index.php?/topic/197082-ckan-the-comprehensive-kerbal-archive-network-v1304-hubble/)
1. Installa la mod [DMP](https://d-mp.org/) attraverso CKAN
    1. In _Filter by name_ cerca **DMP**.
    1. Spunta DMP.
    1. Clicca su _Apply Changes_.
1. Avvia KSP.
1. Fai il login al server
    - address: `sundevil.pl`
    - port: `12301`

## Missione

L'agenzia spaziale mette a disposizioni i razzi e l'atrezzatura piu moderna. La tua missione e' collaborare con la squadra per far arrivare un laboratorio scientifico e 3 astronauti: un ingegnere, un operatore scientifico ed un pilota su Duna.

Il consiglio e' quello di costruire prima una astronave orbitante Kerbin, rifornirla di carburante e partire per Duna una volta che una finestra di lancio si rende possibile.
