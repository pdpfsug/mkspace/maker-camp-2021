# Missioni Scratch

## Onboarding

Queste missioni non fanno riferimento alle giornate.
Se un partecipante non ha mai utilizzato Scratch segue questa lista.
Una volta completata la lista puo passare alle missioni giornaliere.

1. Registrati su [https://scratch.mit.edu/join]().
1. Scrivi il tuo username Scratch in chat.
1. Acceta l'invito per essere un curatore.
1. Segui il progetto Maker Camp 2021 [https://scratch.mit.edu/studios/30074408/activity]()
1. Crea un nuovo progetto.
1. Completa i primi tutorial.

## Missioni Giornaliere

### 2021-09-13

**Facile:** 

[RIF](https://scratch.mit.edu/projects/274034766/), solo l'inizio.

Crea una presentazione testuale per dare il benvenuto ai turisti su Marte.

Deve contenere minimo:

- 3 scene
- Uno sfondo
- Del testo in ogni scena
- Una musica di sottofondo

**Difficile**

[RIF](https://scratch.mit.edu/projects/115660710)

Crea una breve storia animata.

Deve contenere minimo:

- 1 sprite con 5 costumi che rappresentano il movimento
- Interazione con altri sprite
- Una storia
- Effetti sonori
