# Missione 2021-09-13

## Difficile

[RIF](https://scratch.mit.edu/projects/115660710)

Crea una breve storia animata.

Deve contenere minimo:

- 1 sprite con 5 costumi che rappresentano il movimento
- Interazione con altri sprite
- Una storia
- Effetti sonori
