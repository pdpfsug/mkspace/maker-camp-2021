# Onboarding

Benvenuto se e' la prima volta che utilizzi Scratch inizia da qui.

Qui troverai il materiale utile per prepararti a diventare uno Scratcher.

Finita questa parte potrai inziare a svolgere le missioni.


1. Registrati su [https://scratch.mit.edu/join]().
1. Scrivi il tuo username Scratch in chat.
1. Acceta l'invito per essere un curatore.
1. Segui il progetto Maker Camp 2021 [https://scratch.mit.edu/studios/30074408/activity]()
1. Crea un nuovo progetto.
1. Completa i primi tutorial.
